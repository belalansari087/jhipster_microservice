(function() {
    'use strict';

    angular
        .module('getwayappApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
