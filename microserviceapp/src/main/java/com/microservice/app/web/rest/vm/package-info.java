/**
 * View Models used by Spring MVC REST controllers.
 */
package com.microservice.app.web.rest.vm;
